#./ftdcn_sim --fix-key --start 1 --end 1000 --inc 0.01 > uniform/fix-key-bloom-filter.dat && \
#    ./ftdcn_sim --fix-key --algo 1 --start 1000 --end 20000 --inc 0.01       >> uniform/fix-key-bloom-filter.dat &
#./ftdcn_sim --fix-key --algo 0 --start 1   --end 200   --inc 0.0001     > uniform/fix-key-gap.dat &

./ftdcn_sim --qgen zipf --fix-key --start 1 --end 1000 --inc 0.01 > zipf/fix-key-bloom-filter2.dat && \
    ./ftdcn_sim --qgen zipf --fix-key --algo 1 --start 1000 --end 20000 --inc 0.01       >> zipf/fix-key-bloom-filter2.dat &
./ftdcn_sim --qgen zipf --fix-key --algo 0 --start 1   --end 200   --inc 0.0001     > zipf/fix-key-gap2.dat &
