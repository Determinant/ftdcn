#include <cstdio>
#include <cstring>
#include <vector>
#include <string>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <map>
#include <set>
#include <getopt.h>
#include <ctime>
#include "murmur_hash.h"

using std::vector;
using std::map;
using std::set;
using std::sort;
using std::random_shuffle;
using std::min;
using std::max;
using std::swap;

typedef vector<int> VecUInt_t;

const int MAX_N = 1000;
const int MAX_B = 0x3f3f3f3f;
const double EPS = 1e-6;
const double LN2 = 0.693147180559945;

int dcmp(double x) {
    if (fabs(x) < EPS) return 0;
    return x < 0 ? -1 : 1;
}

void function_not_implemented() {
    fprintf(stderr, "Function Not Implemented!\n");
    exit(1);
}

struct Range {
    int l, r;
    Range() {}
    Range(int _l, int _r) : l(_l), r(_r) {}
    int length() { return r - l + 1; }
};

class Simulation;

typedef void (*KeyGen)(Simulation *sim);

class Query {

    protected:
        const Simulation *sim;
    public:
        virtual ~Query() {}
        virtual void init(const Simulation *sim) = 0;
        virtual void stop() {}
        virtual int gen() = 0;
        virtual Range rgen() { function_not_implemented(); }
};


class Algorithm {

    protected:
        int P;  // Partition size (should be a divisor of B)
        Simulation *sim;

    public:
        int suspect_cnt;
        int dist_cnt;
        int failed_cnt; // Failed attempt (algorithm inefficiency)
        int param;      // Value of parameter (g or s)
        double sph;     // Space Consumption Per Host

        virtual void init(Simulation *sim) = 0;
        virtual bool resolve(int key) = 0;
        virtual void rresolve(Range r) { function_not_implemented(); }
        virtual ~Algorithm() {}
};

class Simulation {

    private:

        Query *qgen;
        Algorithm *algo;
        // Total count for
        int access_cnt[3];
        // [0] Access through an edge (cost 2)
        // [1] Access through an aggregation (cost 4)
        // [2] Access through a core (cost 6)

        int invalid_cnt;// Try to locate a non-existing key
        int opt_num;
        int rlen;

        struct Edge {
            Edge *next;
            int key;
        };

    public:

        int K;      // Aggreation num, which is supposed to be even
        int B;      // Key range. Each key should be an integer with [0, B)
        int M;      // Key num. Note that M <= B
        int N;      // Total host num


        Edge *keys[MAX_N];  // Keys located on each host 
        int *sorted_keys;
        int *all_keys;
        int *keys_head[MAX_N + 1];
        double avg_hop, avg_suspect, avg_failed;

    public:

        ~Simulation() { 
            delete algo;
            delete [] sorted_keys;
            delete [] all_keys;
        }

        void add_key(int key, int id) {
            Edge *p = new Edge;
            p->next = keys[id];
            p->key = key;
            keys[id] = p;
        }

        bool key_exist(int key) {
            // TODO: not examined or debugged
            int l, r, mid;
            for (l = 0, r = M; mid = (l + r) >> 1, r - l > 1;
                    (key < sorted_keys[mid] ? r : l) = mid);
            return sorted_keys[l] == key;
        }

        Simulation(const int _K, int _B, int _M) : K(_K), B(_B), M(_M) {
            assert(!(K & 1));
            assert(M <= B);
            assert(B < MAX_B);
            N = K * K * K / 4;
            sorted_keys = NULL;
        }

#define EDGE_ID(x) ((x) / (K >> 1))
#define AGG_ID(x) ((x) / ((K >> 1) * (K >> 1)))

        void access(int id_from, int id_to) {
            if (id_from == id_to) 
                return;
            if (EDGE_ID(id_from) == EDGE_ID(id_to))
                access_cnt[0]++;
            else if (AGG_ID(id_from) == AGG_ID(id_to))
                access_cnt[1]++;
            else 
                access_cnt[2]++;
        }

        bool check_local(int id, int key) {
            int l, r, mid, *arr = keys_head[id];
            for (l = 0, r = int(keys_head[id + 1] - arr); 
                    mid = (l + r) >> 1, r - l > 1;
                    (key < arr[mid] ? r : l) = mid);
            return arr[l] == key;
        }

        void init(KeyGen dgen, Query *_qgen, Algorithm *_algo) {
            memset(access_cnt, 0, sizeof access_cnt);
            invalid_cnt = 0;
            opt_num = 0;
            memset(keys, 0, sizeof keys);

            dgen(this);
#ifdef DEBUG
            fprintf(stderr, "* keys distributed\n");
#endif

            int *sp = sorted_keys = new int[M];
            int *ap = all_keys = new int[M];
            for (int i = 0; i < N; i++)
            {
                keys_head[i] = ap;
                for (Edge *p = keys[i], *np; p; p = np)
                {
                    np = p->next;
                    *(ap++) = *(sp++) = p->key;
                    delete p;
                }
                sort(keys_head[i], ap);
            }
            keys_head[N] = ap;
#ifdef DEBUG
            assert(sp - sorted_keys == M);
#endif
            sort(sorted_keys, sp);
#ifdef DEBUG
            fprintf(stderr, "* all_keys && sorted_keys built\n");
            fprintf(stderr, "* invoking algorithm init\n");
#endif
            (algo = _algo)->init(this); // Attach and init the algorithm
            (qgen = _qgen)->init(this);
#ifdef DEBUG
            fprintf(stderr, "* init done\n");
#endif
        }

        void stop() {
            avg_hop = total_cost() / double(opt_num);
            avg_suspect = algo->suspect_cnt / double(opt_num);
            avg_failed = algo->failed_cnt / double(opt_num);
            qgen->stop();
        }

        void tick() {
            int qkey = qgen->gen();
            bool valid = true;
            if (!key_exist(qkey))
            {
                valid = false; 
                invalid_cnt++;
            }
            bool flag = algo->resolve(qkey);
            assert(flag == valid);
            opt_num++;
        }

        void rtick() {
            Range r = qgen->rgen();
            algo->rresolve(r);
            opt_num++;
            rlen = r.length();
        }

        int total_cost() {
            int total = 0;
            for (int i = 0; i < 3; i++)
                total += access_cnt[i] * (i + 1) * 2;
            return total;
        }

        void print() {
            printf("%u\t%u\t%u\t%u\t%.2f\t%.2f\t%.2f\t%u\t%u\t%u\t%u\t%.3f\t%d\n",
                    K, N, B, M, avg_hop, avg_suspect, avg_failed, 
                    access_cnt[0], access_cnt[1], access_cnt[2],
                    algo->param, algo->sph, rlen);
            fflush(stdout);
        }
};

void uniform_keygen(Simulation *sim) {
    int *arr = new int[sim->B];
    for (int i = 0; i < sim->B; i++)
        arr[i] = i;
    random_shuffle(arr, arr + sim->B);
    for (int i = 0; i < sim->M; i++)
        sim->add_key(arr[i], rand() % sim->N);
    delete [] arr;
}

class UniformQuery : public Query {

    private:
        double len;
        int alen;

    public:
    UniformQuery(double _len) : len(_len) {}

    void init(const Simulation *_sim) {
        sim = _sim;
        alen = max(int(sim->B * len), 1);
        if (!alen) alen = 1;
    }

    int gen() {
        return sim->sorted_keys[rand() % (sim->M)];
    }

    Range rgen() {
        int l = rand() % (sim->B - alen + 1);
        return Range(l, l + alen - 1);
    }
};

class ZipfQuery : public Query {
    int size;
    double *bound;
    void init(const Simulation *_sim) {
        sim = _sim;
        size = sim->M;
        double a = 0;
        for (int i = size; i >= 1; i--)
            a += 1.0 / i;
        a = 1 / a;
        bound = new double[size];
        bound[0] = a;
        for (int i = 1; i < size; i++)
            bound[i] = bound[i - 1] + a / (i + 1);
    }

    void stop() {
        delete bound;
    }

    int gen() {
        int l, r, m;
        double x = rand() / double(RAND_MAX);
        for (l = 0, r = size; (m = (l + r) >> 1), r - l > 1; 
            (x >= bound[m - 1] ? l : r) = m);
        return sim->sorted_keys[l];
    }
};

class GapAlgorithm : public Algorithm {
    public:
        class Block {
            public:
                int owner, l, r;
                int edge_id, agg_id;
                //        double fill;    // Fill factor
                Block(int _owner, int _l, int _r, 
                        const Simulation *sim) : \
                    owner(_owner), l(_l), r(_r) {
                        int K = sim->K;
                        edge_id = EDGE_ID(owner);
                        agg_id= AGG_ID(owner);
                    }

                bool operator<(const Block &other) const {
                    return l < other.l;
                }
        };

        struct Gap {
            int idx, len;
            Gap() {}
            Gap(int _idx, int _len) : idx(_idx), len(_len) {}
            bool operator<(const Gap &b) const {
                return len > b.len;
            }
        };

        typedef vector<Block> VecBlock_t;

    private:

        VecBlock_t gindex[MAX_N];
        int G;  // Block division

    public:


        GapAlgorithm(int _G): G(_G) { param = G; }

        void discover_partition(const VecUInt_t &block,
                int owner, int sowner) {
            int bs = int(block.size());
            if (!bs) return;

            bool *split = new bool[bs];
            memset(split, 0, bs);

            Gap *gaps = new Gap[bs - 1];
            for (int i = 0; i < bs - 1; i++)
                gaps[i] = Gap(i, block[i + 1] - block[i]);
            sort(gaps, gaps + bs - 1);

            int num = min(G, bs - 1);
            while (num && !gaps[num - 1].len) num--;
            for (int i = 0; i < num; i++)
                split[gaps[i].idx] = true;
            split[bs - 1] = true;
            delete [] gaps;
            for (int i = 0; i < bs; i++)
            {
                int pi = i;
                while (!split[i]) i++;
                gindex[sowner].push_back(Block(owner, 
                            block[pi], block[i], sim));
            }
            dist_cnt += num + 1;
            delete [] split;
        }

        void init(Simulation *_sim) {
            sim = _sim;
#ifdef DEBUG
            assert(!(sim->B % sim->N));
#endif
            P = sim->B / sim->N;
            for (int i = 0; i < sim->N; i++)
                gindex[i].clear();
            suspect_cnt = 0;
            dist_cnt = 0;
            failed_cnt = 0;

            for (int i = 0; i < sim->N; i++)
            {
                //int l = P * i, r = l + P;
                // host i should have possesed [l, r) data
                int sowner = -1;
                VecUInt_t block;
                block.clear();
                for (int *it = sim->keys_head[i]; it < sim->keys_head[i + 1]; it++)
                {
                    // should be visited in ascending order
                    int key= *it;
                    if (key / P > sowner)
                    {
                        discover_partition(block, i, sowner);
                        block.clear();
                        sowner = key / P;
                    }
                    block.push_back(key);
                }
                discover_partition(block, i, sowner);
            }
            sph = 0;
            for (int i = 0; i < sim->N; i++)
            {
                sort(gindex[i].begin(), gindex[i].end());
                sph += double(gindex[i].size());
            }
            sph = (sph / sim->N) * (4 + 4 + 4 + 4);
        }

        static bool suspect_cmp(const Block *a, const Block *b) {
            return a->owner < b->owner;
        }

        typedef vector<const Block*> VecBlockPtr_t;

        bool resolve(int key) {
            int s0 = key / P;
            // the actual first hop is ignored
            const VecBlock_t &ref = gindex[s0];
            VecBlockPtr_t suspects;
            suspects.clear();
            for (size_t i = 0; i < ref.size(); i++)
            {
                const Block &cur = ref[i];
                if (cur.l <= key && key <= cur.r)
                    suspects.push_back(&cur);
            }
            sort(suspects.begin(), suspects.end(), GapAlgorithm::suspect_cmp);
#ifdef DEBUG
            assert(suspects.size() == unique(suspects.begin(), suspects.end()) - suspects.begin());
            assert(suspects.size() <= sim->N*2);
#endif
            suspect_cnt += int(suspects.size());
            for (size_t i = 0; i < suspects.size(); i++)
            {
                int id = suspects[i]->owner;
                sim->access(s0, id);
                if (sim->check_local(id, key))
                    return true;
                else 
                    failed_cnt++;
                s0 = id;
            }
            return false;
        }

        void rresolve(Range range) {
            VecUInt_t suspects, all;
            int start = range.l / P, end = range.r / P;
            assert(start < sim->N);
            assert(end < sim->N);
            all.clear();
            for (int host = start; host <= end; host++)
            {
                // ignore the first hop
                const VecBlock_t &ref = gindex[host];
                suspects.clear();
                for (size_t i = 0; i < ref.size(); i++)
                {
                    const Block &cur = ref[i];
                    if (!(range.r < cur.l || range.l > cur.r))
                        suspects.push_back(cur.owner);
                }
                sort(suspects.begin(), suspects.end());
                suspects.resize(unique(suspects.begin(), suspects.end()) - suspects.begin());
                all.insert(all.end(), suspects.begin(), suspects.end());
                suspect_cnt += int(suspects.size());
                if (host < end) sim->access(host, host + 1);
            }
            sort(all.begin(), all.end());
            all.resize(unique(all.begin(), all.end()) - all.begin());
            int s0 = end;
            for (size_t i = 0; i < all.size(); i++)
            {
                int id = all[i];
                sim->access(s0, id);
                s0 = id;
            }
        }
};

class BloomAlgorithm: public Algorithm {

    public:

        class Filter : public GapAlgorithm::Block {
            public:
                vector<bool> bitset;
                int size;
                int hash_num;

                Filter(int _owner, int _size, const Simulation *sim) \
                    : Block(_owner, -1, MAX_B, sim), size(_size) { 
                        bitset.resize(size);
                        fill(bitset.begin(), bitset.end(), 0);
                        hash_num = max(int(floor(size / (double(sim->B) / sim->N) * LN2)), 1);
//                        assert(hash_num == 1);
                    }
                void add(int id) {
                    static unsigned int hash[4];
                    if (id < l) l = id;
                    if (id > r) r = id;
                    MurmurHash3_x64_128(&id, sizeof(id), 0, hash); 
                    unsigned int hv = hash[0];
                    for (int i = 0; i < hash_num; i++, hv += hash[1])
                        bitset[hv % size] = 1;
                }
                bool hit(int id) const {
                    static unsigned int hash[4];
                    MurmurHash3_x64_128(&id, sizeof(id), 0, hash); 
                    unsigned int hv = hash[0];
                    for (int i = 0; i < hash_num; i++, hv += hash[1])
                        if (!bitset[hv % size]) return false;
                    return true;
                }
        };

        typedef vector<Filter> VecFilter_t;

    private:
        VecFilter_t gindex[MAX_N];
        int S;
    public:

        BloomAlgorithm(int _S) : S(_S) { param = S; }

        void discover_partition(const VecUInt_t &block,
                int owner, int sowner) {
            int bs = int(block.size());
            if (!bs) return;

            Filter filter(owner, S, sim);
            for (int i = 0; i < bs; i++)
                filter.add(block[i]);
            gindex[sowner].push_back(filter);
        }

        void init(Simulation *_sim) {
            sim = _sim;
#ifdef DEBUG
            assert(!(sim->B % sim->N));
#endif
            P = sim->B / sim->N;
            for (int i = 0; i < sim->N; i++)
                gindex[i].clear();
            suspect_cnt = 0;
            dist_cnt = 0;
            failed_cnt = 0;

            for (int i = 0; i < sim->N; i++)
            {
                int sowner = -1;
                VecUInt_t block;
                block.clear();
                for (int *it = sim->keys_head[i]; it < sim->keys_head[i + 1]; it++)
                {
                    // should be visited in ascending order
                    int key= *it;
                    if (key / P > sowner)
                    {
                        discover_partition(block, i, sowner);
                        block.clear();
                        sowner = key / P;
                    }
                    block.push_back(key);
                }
                discover_partition(block, i, sowner);
            }
            sph = 0;
            for (int i = 0; i < sim->N; i++)
                sph += double(gindex[i].size());
            sph = (sph / sim->N) * (4 + 4 + 4 + 4 + S / 8.0);
        }

        typedef vector<const Filter*> VecFilterPtr_t;

        bool resolve(int key) {
            int s0 = key / P;
            // ignore the first hop
            //        sim->access(rand() % sim->N, s0); // start from an arbitary host
            //        if (sim->check_local(s0, key))
            //            return true;
            const VecFilter_t &ref = gindex[s0];
            VecFilterPtr_t suspects;
            suspects.clear();
            // TODO: replace the following brute-force code with binary search
            for (size_t i = 0; i < ref.size(); i++)
            {
                const Filter &cur = ref[i];
                if (cur.l <= key && key <= cur.r && cur.hit(key))
                    suspects.push_back(&cur);
            }
            sort(suspects.begin(), suspects.end(), GapAlgorithm::suspect_cmp);
#ifdef DEBUG
            assert(suspects.size() == unique(suspects.begin(), suspects.end()) - suspects.begin());
            assert(suspects.size() <= sim->N*2);
#endif
            suspect_cnt += int(suspects.size());
            for (size_t i = 0; i < suspects.size(); i++)
            {
                int id = suspects[i]->owner;
                sim->access(s0, id);
                if (sim->check_local(id, key))
                    return true;
                else 
                    failed_cnt++;
                s0 = id;
            }
            return false;
        }
};

int opt_num = 500000;
double alpha = 0.8;
Query *qgen = new UniformQuery(0);

void print_head() {
    printf("### FTDCN Simulation Result (Iteration = %d)\n", opt_num);
    printf("## Hop\t= Average Hop\n"
            "## Susp\t= Average Suspect\n"
            "## Failed\t= Average Failed Trails Before a Success\n"
            "## D\t= [0, B)\n"
          );
    //                    "Size = Average Space cosumed by the Algorithm (in bytes)\n"
    printf("### k\tn\tB\tm\tHop\tSusp\tFailed\t2\t4\t6\tg/s\tsph\tr\n");
}

void fix_attempt(int argc, char **argv) {

    static struct option lopts[] = 
    {
        { "algo",   required_argument,    0,  'a'},
        { "start",  required_argument,   0,  's'},
        { "end",    required_argument,   0,  'e'},
        { "inc",    required_argument,   0,  'i'},
        { "max",   required_argument,   0,  'm'},
        { "thres",  required_argument,   0,  'l'},
        {0, 0, 0, 0}
    };

    double thres = 0.1;
    int max = 10000;
    bool algo = 1;
    int k = 8;
    int start = 10000, end = 100000, inc = 10000;

    while (1)
    {
        int opt_index = -1;
        int c = getopt_long(argc, argv, "a:s:e:i:k:", lopts, &opt_index);
        if (c == -1)
            break;
        switch (c)
        {
            case 'a': 
                algo = atoi(optarg);
                break;
            case 's':
                start = atoi(optarg);
                break;
            case 'e':
                end = atoi(optarg);
                break;
            case 'i':
                inc = atoi(optarg);
                break;
            case 'k':
                k = atoi(optarg);
                break;
            case 'l':
                thres = atof(optarg);
                break;
            case 'm':
                max = atoi(optarg);
        }
    }

    printf("### Fix Attempt(= Failed + 1) Mode\n"
            "## algo = %d, thres = %.2f, start = %d, end = %d,\n" 
            "## inc = %d, max = %d\n",
            algo, thres, start, end, inc, max);

    if (algo)
    {
        int s = 10;
        for (int kph = start; kph <= end; kph += inc)
        {
            int b = k * k * k / 4 * kph;
            int l, r, m;
            for (l = 0, r = max; m = (l + r) >> 1, r - l > 1;)
//            for (;; s += step)
            {
                Simulation *sim = new Simulation(k, b, int(b * alpha));
                sim->init(uniform_keygen, qgen, new BloomAlgorithm(m));
                for (int i = 0; i < opt_num; i++)
                    sim->tick();
                sim->stop();
                if (dcmp(sim->avg_failed / double(sim->N) - thres) < 0)
                {
                    r = m;
//                    sim->print();
//                    delete sim;
//                    break;
                }
                else l = m;
                delete sim;
            }
            Simulation *sim = new Simulation(k, b, int(b * alpha));
            sim->init(uniform_keygen, qgen, new BloomAlgorithm(r));
            for (int i = 0; i < opt_num; i++)
                sim->tick();
            sim->stop();
            sim->print();
        }
    }
    else
    {
        int s = 0;
        for (int kph = start; kph <= end; kph += inc)
        {
            int b = k * k * k / 4 * kph;
            for (;; s += 1)
            {
                Simulation *sim = new Simulation(k, b, int(b * alpha));
                sim->init(uniform_keygen, qgen, new GapAlgorithm(s));
                for (int i = 0; i < opt_num; i++)
                    sim->tick();
                sim->stop();
                if (sim->avg_failed / double(sim->N) < thres)
                {
                    sim->print();
                    delete sim;
                    break;
                }
                delete sim;
            }
        }
    }
}

void fix_key(int argc, char **argv) {

    static struct option lopts[] = 
    {
        { "algo",  required_argument,    0,  'a'},
        { "start",  required_argument,   0,  's'},
        { "end",    required_argument,   0,  'e'},
        { "inc",    required_argument,   0,  'i'},
        { "kph",    required_argument,   0,  'p'},
        {0, 0, 0, 0}
    };

    bool algo = 1;
    int k = 8;
    int kph = 10000;
    int start = 1, end = int(kph * 0.1), inc = 10;

    while (1)
    {
        int opt_index = -1;
        int c = getopt_long(argc, argv, "a:s:e:i:k:p:", lopts, &opt_index);
        if (c == -1)
            break;
        switch (c)
        {
            case 'a': 
                algo = atoi(optarg);
                break;
            case 's':
                start = atoi(optarg);
                break;
            case 'e':
                end = atoi(optarg);
                break;
            case 'i':
                inc = int(kph * atof(optarg));
                break;
            case 'k':
                k = atoi(optarg);
                break;
            case 'p':
                kph = atoi(optarg);
        }
    }

    printf("### Fix Key(m) Mode\n"
            "## algo = %d, start = %d, end = %d,\n" 
            "## inc = %d\n",
            algo, start, end, inc);

    int b = k * k * k / 4 * kph;
    if (algo)
    {
        for (int s = start; s <= end; s += inc)
        {
            Simulation *sim = new Simulation(k, b, int(b * alpha));
            sim->init(uniform_keygen, qgen, new BloomAlgorithm(s));
            for (int i = 0; i < opt_num; i++)
                sim->tick();
            sim->stop();
            sim->print();
            delete sim;
        }
    }
    else
    {
        for (int s = start; s <= end; s += inc)
        {
            Simulation *sim = new Simulation(k, b, int(b * alpha));
            sim->init(uniform_keygen, qgen, new GapAlgorithm(s));
            for (int i = 0; i < opt_num; i++)
                sim->tick();
            sim->stop();
            sim->print();
            delete sim;
        }
    }
}

void fix_gap(int argc, char **argv) {

    static struct option lopts[] = 
    {
        { "gap",    required_argument,   0,  'g'},
        {0, 0, 0, 0}
    };

    int k = 8, g = 0;

    while (1)
    {
        int opt_index = -1;
        int c = getopt_long(argc, argv, "g:", lopts, &opt_index);
        if (c == -1)
            break;
        switch (c)
        {
            case 'g': 
                g = atoi(optarg);
        }
    }

    printf("### Fix Gap(g) Mode\n"
            "## g = %d\n", g);

    for (int kph = 1; kph < 10000; kph += 1000)
    {
        int b = k * k * k / 4 * kph;
        Simulation *sim = new Simulation(k, b, int(b * alpha));
        sim->init(uniform_keygen, qgen, new GapAlgorithm(g));
        for (int i = 0; i < opt_num; i++)
            sim->tick();
        sim->stop();
        sim->print();
        delete sim;
    }
}

void fix_filter(int argc, char **argv) {

    static struct option lopts[] = 
    {
        { "size",    required_argument,   0,  's'},
        {0, 0, 0, 0}
    };

    int k = 8, s = 1;

    while (1)
    {
        int opt_index = -1;
        int c = getopt_long(argc, argv, "s:", lopts, &opt_index);
        if (c == -1)
            break;
        switch (c)
        {
            case 's': 
                s = atoi(optarg);
        }
    }

    printf("### Fix Size(s) Mode\n"
            "## s = %d\n", s);

    for (int kph = 1; kph < 10000; kph += 1000)
    {
        int b = k * k * k / 4 * kph;
        Simulation *sim = new Simulation(k, b, int(b * alpha));
        sim->init(uniform_keygen, qgen, new BloomAlgorithm(s));
        for (int i = 0; i < opt_num; i++)
            sim->tick();
        sim->stop();
        sim->print();
        delete sim;
    }
}


void range(int argc, char **argv) {

    static struct option lopts[] = 
    {
        { "start",  required_argument,   0,  's'},
        { "end",    required_argument,   0,  'e'},
        { "inc",    required_argument,   0,  'i'},
        { "kph",    required_argument,   0,  'p'},
        { "gap",    required_argument,   0,  'g'},
        { "length", required_argument,   0,  'l'},
        {0, 0, 0, 0}
    };

    int g = 50;
    int k = 8;
    int kph = 10000;
    double start = 0, end = 1, inc = 0.1;

    while (1)
    {
        int opt_index = -1;
        int c = getopt_long(argc, argv, "s:e:i:k:l:g:", lopts, &opt_index);
        if (c == -1)
            break;
        switch (c)
        {
            case 's':
                start = atof(optarg);
                break;
            case 'e':
                end = atof(optarg);
                break;
            case 'i':
                inc = atof(optarg);
                break;
            case 'k':
                k = atoi(optarg);
                break;
            case 'p':
                kph = atoi(optarg);
                break;
            case 'g':
                g = atoi(optarg);
                break;
        }
    }

    printf("### Range Mode\n"
            "## algo = 0, start = %f, end = %f,\n" 
            "## inc = %f\n",
            start, end, inc);

    int b = k * k * k / 4 * kph;
    for (double len = start; len <= end; len += inc)
    {
        UniformQuery *_qgen = new UniformQuery(len);
        Simulation *sim = new Simulation(k, b, int(b * alpha));
        sim->init(uniform_keygen, _qgen, new GapAlgorithm(g));
        for (int i = 0; i < opt_num; i++)
            sim->rtick();
        sim->stop();
        sim->print();
        delete sim;
        delete _qgen;
    }
}

void show_help() {
    fprintf(stderr, "To be implemented.\n");
}

int main(int argc, char **argv) {

    srand(unsigned(time(0)));
    int sim_mode = -1;
    static struct option lopts[] = 
    {
        { "fix-attempt",    no_argument,            &sim_mode,  0},
        { "fix-key",        no_argument,            &sim_mode,  1},
        { "range",          no_argument,            &sim_mode,  2},
        { "fix-gap",        no_argument,            &sim_mode,  3},
        { "fix-filter",     no_argument,            &sim_mode,  4},
        { "qgen",           required_argument,      0,          'q'},
        { "help",           no_argument,            0,          'h'},
        {0, 0, 0, 0}
    };
    int opt_index = 0;
    while (sim_mode == -1)
    {
        int c = getopt_long(argc, argv, "h", lopts, &opt_index);
        if (c == -1)
            break;
        switch (c)
        {
            case 0: break;
            case 'h': show_help();
                      break;
            case 'q':
                      if (*optarg == 'z')     // zipf's law
                          qgen = new ZipfQuery();
                      else if (*optarg == 'u')
                          qgen = new UniformQuery(0);
                      else
                      {
                          fprintf(stderr, "Invalid argument: \"%s\" for qgen\n", optarg);
                          return 1;
                      }
        }
    }

    if (sim_mode == -1)
    {
        fprintf(stderr, "Please specify a simulation mode.\n");
        return 1;
    }
    print_head();
    switch (sim_mode) 
    {
        case 0:
            fix_attempt(argc, argv);
            break;
        case 1:
            fix_key(argc, argv);
            break;
        case 2:
            range(argc, argv);
            break;
        case 3:
            fix_gap(argc, argv);
            break;
        case 4:
            fix_filter(argc, argv);
            break;
    }
    return 0;
}
