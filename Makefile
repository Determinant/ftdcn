all: ftdcn_sim

ftdcn_sim: ftdcn_sim.o murmur_hash.o
	g++ -o ftdcn_sim ftdcn_sim.o murmur_hash.o -O2

ftdcn_sim.o: 
	g++ -c ftdcn_sim.cpp -O2

murmur_hash.o:
	g++ -c murmur_hash.cpp -O2

clean:
	rm *.o ftdcn_sim
