./ftdcn_sim --qgen zipf --fix-attempt --algo 1 --thres 0.003 > zipf/fix-attempt-bloom-filter2.dat && \
    ./ftdcn_sim --qgen uniform --fix-attempt --algo 1 --thres 0.003 > uniform/fix-attempt-bloom-filter2.dat &

./ftdcn_sim --qgen zipf --fix-attempt --algo 1 --thres 0.01 > zipf/fix-attempt-bloom-filter.dat && \
    ./ftdcn_sim --qgen uniform --fix-attempt --algo 1 --thres 0.01 > uniform/fix-attempt-bloom-filter.dat &

#./ftdcn_sim --qgen zipf --fix-attempt --algo 0 --thres 0.003 --step 1 > zipf/fix-attempt-gap2.dat && \
#    ./ftdcn_sim --qgen uniform --fix-attempt --algo 0 --thres 0.003 --step 1 > uniform/fix-attempt-gap2.dat &
#
#./ftdcn_sim --qgen zipf --fix-attempt --algo 0 --thres 0.01 --step 1 > zipf/fix-attempt-gap.dat && \
#    ./ftdcn_sim --qgen uniform --fix-attempt --algo 0 --thres 0.01 --step 1 > uniform/fix-attempt-gap.dat &
